import Navbar from "./components/Navbar";
import Chart from "chart.js/auto";

import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import FriendItem from "./components/FriendItem";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

const App = () => {
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [gender, setGender] = useState("");
  const [loading, setLoading] = useState("");
  const [friends, setFriends] = useState([]);
  const [buttonName, setButtonName] = useState("Add");
  const [friendId, setFriendId] = useState("");

  const chartContainer = useRef(null);
  const [chart, setChart] = useState(null);

  const [typeChart, setTypeChart] = useState("pie");
  const [buttonPdf, setButtonPdf] = useState("Cetak PDF");

  const [isDisabled, setIsDisabled] = useState(false);

  const handleSubmit = (e) => {
    setButtonName("Loading...");
    setIsDisabled(true);
    e.preventDefault();
    if (name.trim() === "") {
      alert("Name is required!");
      setButtonName("Add");
      setIsDisabled(false);
      return;
    }
    if (gender.trim() === "") {
      alert("Gender is required!");
      setButtonName("Add");
      setIsDisabled(false);
      return;
    }
    axios
      .post("http://localhost:8000/api/friends/", {
        name,
        gender,
        age,
      })
      .then((response) => {
        getFriends();
        setName("");
        setGender("");
        setAge(0);
        getPercentage();
        setButtonName("Add");
        setIsDisabled(false);
      })
      .catch((error) => {
        alert(error.response.data.message);
        console.log(error);
        setButtonName("Add");
        setIsDisabled(false);
      });
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    setButtonName("Loading...");
    setIsDisabled(true);
    if (name.trim() === "") {
      alert("Name is required!");
      setButtonName("Add");
      setIsDisabled(false);
      return;
    }
    if (gender.trim() === "") {
      alert("Gender is required!");
      setButtonName("Add");
      setIsDisabled(false);
      return;
    }
    axios
      .put(`http://localhost:8000/api/friends/${friendId}`, {
        name,
        gender,
        age,
      })
      .then((response) => {
        getFriends();
        getPercentage();
        setName("");
        setGender("");
        setAge(0);
        setButtonName("Add");
        setIsDisabled(false);
      })
      .catch((error) => {
        alert(error.response.data.message);
        console.log(error);
        setButtonName("Add");
        setIsDisabled(false);
      });
  };

  const getFriends = async () => {
    await axios.get("http://localhost:8000/api/friends/").then((response) => {
      setLoading(false);
      setFriends(response.data);
      console.log(response.data);
    });
  };

  function deleteFriend(id) {
    const confirmed = window.confirm(
      "Apakah Anda yakin ingin menghapus data ini?"
    );
    if (confirmed) {
      axios
        .delete(`http://localhost:8000/api/friends/${id}`)
        .then((response) => {
          getFriends();
          getPercentage();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  function editFriend(friends) {
    setButtonName("Update");

    setName(friends.name);
    setAge(friends.age);
    setGender(friends.gender);
    setFriendId(friends.id);
  }

  const getPercentage = async () => {
    if (chart) {
      chart.destroy(); // destroy previous chart instance
    }
    try {
      const response = await fetch(
        "http://localhost:8000/api/friend/percentage"
      );
      const data = await response.json();

      const friendChart = new Chart(chartContainer.current, {
        type: typeChart,
        data: {
          labels: ["Laki-laki", "Perempuan", "<20 tahun", ">20 tahun"],
          datasets: [
            {
              label: "Persentase Teman",
              data: [
                data.male_percentage,
                data.female_percentage,
                data.under_20_percentage,
                data.above_20_percentage,
              ],
              backgroundColor: ["#f6ad55", "#fc8181", "#48bb78", "#63b3ed"],
            },
          ],
        },
      });

      setChart(friendChart);
    } catch (error) {
      console.error(error);
    }

    return () => {
      if (chart) {
        chart.destroy();
      }
    };
  };

  useEffect(() => {
    setLoading(true);
    getFriends();
    getPercentage();
  }, []);

  const changeTypeChart = (e) => {
    console.log(e.target.value);
    console.log(typeChart);
    setTypeChart(e.target.value);
    getPercentage();
  };

  const exportToPdf = async () => {
    setButtonPdf("Loading...");
    setIsDisabled(true);
    const chartCanvas = document.getElementById("chart");
    const chartImage = await html2canvas(chartCanvas);

    const tableCanvas = document.getElementById("table");
    const tableImage = await html2canvas(tableCanvas);

    const pdf = new jsPDF("p", "mm", "a4");

    // pdf.setFontSize(20); // set font size
    // pdf.setFont("helvetica", "bold"); // set font type and weight
    pdf.text("Data Teman", 10, 15);
    pdf.addImage(tableImage, "PNG", 10, 20, 180, 100);
    pdf.addImage(chartImage, "PNG", 50, 130, 100, 100);

    pdf.save("laporan-data-teman.pdf");
    setButtonPdf("Cetak PDF");
    setIsDisabled(false);
  };

  return (
    <div className="App">
      <Navbar />
      <main className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
        <div className="mt-10">
          <form
            className="flex items-center justify-center gap-3 mb-8"
            onSubmit={buttonName === "Add" ? handleSubmit : handleUpdate}
          >
            <div className="mb-4">
              <label
                className="block text-gray-700 font-bold mb-2"
                htmlFor="name"
              >
                Nama
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="name"
                type="text"
                placeholder="Enter nama"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 font-bold mb-2"
                htmlFor="gender"
              >
                Jenis Kelamin
              </label>
              <select
                name="gender"
                id="gender"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
                className="border border-gray-300 rounded-md px-4 py-2"
              >
                <option value="">-- Pilih Jenis Kelamin --</option>
                <option value="male">Laki-laki</option>
                <option value="female">Perempuan</option>
              </select>
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 font-bold mb-2"
                htmlFor="age"
              >
                Usia
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="age"
                type="number"
                min="0"
                placeholder="Enter usia"
                value={age}
                onChange={(e) => setAge(e.target.value)}
              />
            </div>
            <div className="mt-3">
              <button
                disabled={isDisabled}
                className={`bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline ${
                  isDisabled ? "opacity-50 cursor-not-allowed" : ""
                }`}
                type="submit"
              >
                {buttonName}
              </button>
            </div>
          </form>
        </div>
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 p-10">
            <div className="flex justify-between items-center mb-4">
              <h1 className="text-2xl font-bold mb-4">Data Teman</h1>
              <button
                disabled={isDisabled}
                className={`bg-green-500 hover:bg-green-700 text-sm text-white font-bold px-2 py-2 rounded focus:outline-none focus:shadow-outline ${
                  isDisabled ? "opacity-50 cursor-not-allowed" : ""
                }`}
                type="submit"
                onClick={exportToPdf}
              >
                {buttonPdf}
              </button>
            </div>
            <table
              id="table"
              className="min-w-full table-auto border-collapse border border-gray-300 divide-y divide-gray-200"
            >
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-4 py-2 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    #
                  </th>
                  <th
                    scope="col"
                    className="px-4 py-2 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Nama
                  </th>
                  <th
                    scope="col"
                    className="px-4 py-2 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Jenis Kelamin
                  </th>
                  <th
                    scope="col"
                    className="px-4 py-2 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Usia
                  </th>
                  <th
                    scope="col"
                    className="px-4 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {loading ? (
                  "Loading..."
                ) : (
                  <>
                    {friends.length <= 0 ? (
                      <tr className="">
                        <td colSpan={4}>
                          <p className="text-yellow-900 underline italic mt-5">
                            Tidak ada data
                          </p>
                        </td>
                      </tr>
                    ) : (
                      <>
                        {friends.map((friend, index) => (
                          <FriendItem
                            index={index}
                            key={friend.id}
                            friend={friend}
                            onDelete={deleteFriend}
                            onEdit={editFriend}
                          />
                        ))}
                      </>
                    )}
                  </>
                )}
              </tbody>
            </table>
          </div>
          <div className="w-full md:w-1/2 p-10">
            <div className="flex justify-end mb-3">
              <select
                name="type-chart"
                id="type-chart"
                value={typeChart}
                onChange={changeTypeChart}
                className="border border-gray-300 rounded-md px-4 py-2"
              >
                <option value="pie">Pie Chart</option>
                <option value="bar">Bar Chart</option>
                <option value="line">Line Chart</option>
              </select>
            </div>
            <canvas id="chart" ref={chartContainer} />
          </div>
        </div>
      </main>
    </div>
  );
};
export default App;
