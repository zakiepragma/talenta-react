import React from "react";

const FriendItem = ({ index, friend, onDelete, onEdit }) => {
  return (
    <tr>
      <td className="whitespace-nowrap border px-2 py-1 text-center">
        {index + 1}
      </td>
      <td className="whitespace-nowrap border px-2 py-1">{friend.name}</td>
      <td className="whitespace-nowrap border px-2 py-1">
        {friend.gender === "male" ? "Laki-laki" : "Perempuan"}
      </td>
      <td className="whitespace-nowrap border px-2 py-1">
        {friend.age + " tahun"}
      </td>
      <td className="whitespace-nowrap border py-1 gap-2 flex justify-center">
        <button
          onClick={() => onEdit(friend)}
          className="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-2 rounded-full focus:outline-none focus:shadow-outline"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M15 3h3v3M7.35 14.849l8.485-8.485 1.414 1.414-8.485 8.485H7.35v-1.414zm-1.768 2.828A1 1 0 005 18h2.121l-1.414-1.414z"
            />
          </svg>
        </button>
        <button
          onClick={() => onDelete(friend.id)}
          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-2 rounded-full focus:outline-none focus:shadow-outline"
        >
          <svg
            className="h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </button>
      </td>
    </tr>
  );
};

export default FriendItem;
